# Por que trabalhar na Zoop?

A Zoop é uma startup que apesar de jovem, vem revolucionando o mercado de Fintech, com uma plataforma 100% white-label de pagamento eletrônico, totalmente customizável e extensível através de SDKs, APIs, além de pacotes de hardware, software, switch de pagamento e apoio, através de uma infraestrutura única: payment Platform as a Service (pPaaS). 

Se você é apaixonado(a) por tecnologia, rejeita rótulos, adora hardware, é louco(a) por software e maluco(a) o bastante para achar que pode mudar o mundo e revolucionar o mercado de pagamentos global, então você é Zoop. o/

Estamos buscando devs-empreendedores(as) que não tenham medo de sujar as mãos de graxa e colocar a mão na massa. Aquelas pessoas que não esperam acontecer, que são proativas e gostam principalmente de resolver problemas (tranquilo, temos problemas - e muitos - para resolver ;) ).

![Queremos você](/img/we-want-you.jpg?raw=true)

# Quero trabalhar na Zoop, como faço?

Visite-nos no AngelList: https://angel.co/zoop-1/jobs e submeta sua aplicação, explicando seu interesse / motivações para fazer parte do nosso time.

Dúvidas? Entre em contato através do e-mail jobs@pagzoop.com

# Quais vagas estão abertas?

## Software Engineer, Web Platform

Estamos selecionando devs que dominem tecnologias e plataformas Web para atuar no desenvolvimento de nossas APIs, plataformas de pagamento e transferências eletrônicas.

SKILLS: PHP, Ruby, Python, Javascript, jQuery, Android, RESTful Services, Linux, Mac, Git, Mobile Payments, Dev Ops, HTML5 & CSS3, REST APIs, SQL, MySQL, PostgreSQL

Diferencial: Cloud / Amazon AWS/VPC, Frontend (JavaScript full, otimizações em geral e não copy and paste. :) )

## Android Developer

Estamos selecionando devs que dominem Android para atuar no desenvolvimento e manutenção de nossas soluções móveis e SDKs.

SKILLS: Java, Linux, Mac, Git, Mobile Application Design, Payments, Android, Mobile Development, Mobile Application Development

Diferencial: iOS é um baita plus, se for essa pessoa, vem com a gente. :)


# Condições e Benefícios Oferecidos

* Local: Rio de Janeiro
* Regime: CLT / PJ
* Período: Integral
* Contratação imediata

Oferecemos salário de mercado!